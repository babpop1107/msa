package com.jogym.api.member.service;

import com.jogym.api.member.entity.StoreMember;
import com.jogym.common.exception.CAccessDeniedException;
import com.jogym.common.exception.CMissingDataException;
import com.jogym.api.member.model.ProfileResponse;
import com.jogym.api.member.repository.StoreMemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfileService {
    private final StoreMemberRepository storeMemberRepository;

    public StoreMember getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName(); // 토큰으로 아이디값 추출
        StoreMember storeMember = storeMemberRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기

        if (!storeMember.getIsEnabled()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return storeMember;
    }

    public ProfileResponse getProfile() {
        StoreMember storeMember = getMemberData();
        return new ProfileResponse.ProfileResponseBuilder(storeMember).build();
    }
}
