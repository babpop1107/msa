package com.jogym.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class AdminUpdateRequest {
    @ApiModelProperty(notes = "이름(2~20)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(notes = "연락처(8~20)", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String phoneNumber;
}
