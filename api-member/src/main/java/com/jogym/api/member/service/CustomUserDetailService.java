package com.jogym.api.member.service;

import com.jogym.api.member.entity.StoreMember;
import com.jogym.common.exception.CMissingDataException;
import com.jogym.api.member.repository.StoreMemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final StoreMemberRepository storeMemberRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        StoreMember storeMember = storeMemberRepository.findByUsername(username).orElseThrow(CMissingDataException::new);
        return  storeMember;
    }
}
