package com.jogym.api.member.service;

import com.jogym.api.member.entity.StoreMember;
import com.jogym.common.exception.CMissingDataException;
import com.jogym.api.member.repository.StoreMemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StoreMemberService {
    private final StoreMemberRepository storeMemberRepository;

    public StoreMember setStore(long id) {
        return storeMemberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
}
