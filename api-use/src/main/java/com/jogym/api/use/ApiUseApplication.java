package com.jogym.api.use;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiUseApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiUseApplication.class, args);
    }
}
