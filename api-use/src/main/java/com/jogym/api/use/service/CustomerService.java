package com.jogym.api.use.service;

import com.jogym.api.use.entity.Customer;
import com.jogym.api.use.repository.CustomerRepository;
import com.jogym.common.exception.CMissingDataException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public Customer getData(long customerId) {
        return customerRepository.findById(customerId).orElseThrow(CMissingDataException::new);
    }

}
