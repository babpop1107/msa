package com.jogym.api.use.repository;

import com.jogym.api.use.entity.SeasonTicket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeasonTicketRepository extends JpaRepository<SeasonTicket, Long> {

}
