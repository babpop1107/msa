package com.jogym.api.use.repository;

import com.jogym.api.use.entity.PtTicketUsedHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PtTicketUsedHistoryRepository extends JpaRepository<PtTicketUsedHistory, Long> {
}
