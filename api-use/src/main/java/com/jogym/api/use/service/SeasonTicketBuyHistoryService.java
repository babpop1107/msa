package com.jogym.api.use.service;

import com.jogym.api.use.entity.SeasonTicketBuyHistory;
import com.jogym.api.use.repository.SeasonTicketBuyHistoryRepository;
import com.jogym.common.exception.CMissingDataException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SeasonTicketBuyHistoryService {
    private final SeasonTicketBuyHistoryRepository seasonTicketBuyHistoryRepository;

    public SeasonTicketBuyHistory getData(long id) {
        return seasonTicketBuyHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

}

