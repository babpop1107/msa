package com.jogym.api.use.controller;

import com.jogym.api.use.entity.Customer;
import com.jogym.api.use.entity.SeasonTicketBuyHistory;
import com.jogym.api.use.model.seasonticketusedhistory.SeasonTicketUsedHistoryItem;
import com.jogym.api.use.service.CustomerService;
import com.jogym.api.use.service.SeasonTicketBuyHistoryService;
import com.jogym.api.use.service.SeasonTicketService;
import com.jogym.api.use.service.SeasonTicketUsedHistoryService;
import com.jogym.common.response.model.CommonResult;
import com.jogym.common.response.model.ListResult;
import com.jogym.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "정기권 이용내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/season-ticket-used-history")
public class SeasonTicketUsedHistoryController {
    private final SeasonTicketUsedHistoryService seasonTicketUsedHistoryService;
    private final SeasonTicketBuyHistoryService seasonTicketBuyHistoryService;
    private final CustomerService customerService;
    private final SeasonTicketService seasonTicketService;

    @ApiOperation(value = "정기권 이용내역 등록")
    @PostMapping("/season-ticket-buy-history-id/{SeasonTicketBuyHistoryId}")
    public CommonResult setSeasonTicketUsedHistory(@PathVariable long SeasonTicketBuyHistoryId) { // 정기권 이용내역을 등록시키기 위해선 정기권 구매내역 id가 필요하다.

         SeasonTicketBuyHistory seasonTicketBuyHistory = seasonTicketBuyHistoryService.getData(SeasonTicketBuyHistoryId);
        // 정기권 구매내역 id를 가져오기 위해 seasonTicketBuyHistoryService에 id를 주면 return으로 원본을 주는 기능을 만들고 가져와서 사용한다.

        seasonTicketUsedHistoryService.setSeasonTicketUsedHistory(seasonTicketBuyHistory);
        // 정기권 이용내역 서비스를 끌고와 등록 기능으로 이름 정의해두었던 setSeasonTicketUsedHistory를 가져오고 은
        // 기능을 수행시키는 데 있어 필요하기 때문에 아이디를 주면 원본을 주는 친구를 통해 얻은 구매내역 원본을 가져와 칸에 채워준다.

        return ResponseService.getSuccessResult();
        // 소스코드가 온전히 잘 수행되었을 때 SuccessResult에 담긴 성공했다는 메시지를 넘긴다.
    }

//    @ApiOperation(value = "정기권 이용내역 리스트") // 문제 존나 많음
//    @GetMapping("/member-id/{customerId}/season-ticket-id/{seasonTicketId}")
//    public ListResult<SeasonTicketUsedHistoryItem> getData(@RequestParam(value = "page", required = false, defaultValue = "1")int page, @PathVariable long customerId) {
//        Customer customer = customerService.getData(customerId);
//        return ResponseService.getListResult(seasonTicketUsedHistoryService.getData(page, customer), true);
//    }
}
