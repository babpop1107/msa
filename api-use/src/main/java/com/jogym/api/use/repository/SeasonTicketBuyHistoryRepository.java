package com.jogym.api.use.repository;

import com.jogym.api.use.entity.SeasonTicketBuyHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeasonTicketBuyHistoryRepository extends JpaRepository<SeasonTicketBuyHistory, Long> {


}
