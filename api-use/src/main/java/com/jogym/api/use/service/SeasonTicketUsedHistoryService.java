package com.jogym.api.use.service;

import com.jogym.api.use.entity.Customer;
import com.jogym.api.use.entity.SeasonTicketBuyHistory;
import com.jogym.api.use.entity.SeasonTicketUsedHistory;
import com.jogym.common.exception.CAlreadyDateCheckTodayException;
import com.jogym.common.response.model.ListResult;
import com.jogym.api.use.model.seasonticketusedhistory.SeasonTicketUsedHistoryItem;
import com.jogym.api.use.repository.SeasonTicketUsedHistoryRepository;
import com.jogym.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SeasonTicketUsedHistoryService {
    private final SeasonTicketUsedHistoryRepository seasonTicketUsedHistoryRepository;

    public void setSeasonTicketUsedHistory(SeasonTicketBuyHistory seasonTicketBuyHistory) {
        if (!checkToday(seasonTicketBuyHistory.getCustomer())) throw new CAlreadyDateCheckTodayException();
        SeasonTicketUsedHistory seasonTicketUsedHistory = new SeasonTicketUsedHistory.SeasonTicketUsedHistoryBuilder(seasonTicketBuyHistory).build();

        seasonTicketUsedHistoryRepository.save(seasonTicketUsedHistory);
    }

    public ListResult<SeasonTicketUsedHistoryItem> getData(int page, Customer customer) {
        Page<SeasonTicketUsedHistory> originList = seasonTicketUsedHistoryRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));
        List<SeasonTicketUsedHistoryItem> result = new LinkedList<>();
        for (SeasonTicketUsedHistory seasonTicketUsedHistory : originList) {
            result.add(new SeasonTicketUsedHistoryItem.Builder(customer, seasonTicketUsedHistory).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private boolean checkToday(Customer customer){
        Optional<SeasonTicketUsedHistory> check = seasonTicketUsedHistoryRepository.findBySeasonTicketBuyHistory_CustomerAndDateLast(customer, LocalDate.now());

        if (check.isPresent()) return false;
        return true;
    }
}
