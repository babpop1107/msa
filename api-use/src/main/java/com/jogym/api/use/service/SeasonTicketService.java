package com.jogym.api.use.service;

import com.jogym.api.use.entity.SeasonTicket;
import com.jogym.api.use.repository.SeasonTicketRepository;
import com.jogym.common.exception.CMissingDataException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SeasonTicketService {
    private final SeasonTicketRepository seasonTicketRepository;

    public SeasonTicket getOriginSeasonTicket(long id) {
        return seasonTicketRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

}
