package com.jogym.api.goods.repository;

import com.jogym.api.goods.entity.SeasonTicket;
import com.jogym.api.goods.entity.StoreMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeasonTicketRepository extends JpaRepository<SeasonTicket, Long> {
    Page<SeasonTicket> findAllByStoreMemberAndIsEnabledOrderByDateCreateDesc(StoreMember storeMember, Boolean isEnable, Pageable pageable);

    //Boolean existsBy


}
