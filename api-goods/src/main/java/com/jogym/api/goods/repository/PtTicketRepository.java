package com.jogym.api.goods.repository;

import com.jogym.api.goods.entity.PtTicket;
import com.jogym.api.goods.entity.StoreMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PtTicketRepository extends JpaRepository<PtTicket, Long> {

    Page<PtTicket> findAllByStoreMemberAndIsEnabledOrderByDateCreate(StoreMember storeMember, Boolean isEnable, Pageable pageable);

}
