package com.jogym.api.goods.repository;

import com.jogym.api.goods.entity.StoreMember;
import com.jogym.api.goods.entity.Trainer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TrainerRepository extends JpaRepository<Trainer, Long> {
    Page<Trainer> findAllByStoreMemberAndIsEnabledOrderByDateCreateDesc(StoreMember storeMember, Boolean isEnable, Pageable pageable);
    Optional<Trainer> findByIdAndStoreMember(Long id, StoreMember storeMember);
}
