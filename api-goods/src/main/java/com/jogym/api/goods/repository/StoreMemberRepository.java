package com.jogym.api.goods.repository;

import com.jogym.api.goods.entity.StoreMember;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StoreMemberRepository extends JpaRepository<StoreMember, Long> {
    Optional<StoreMember> findByUsername(String username);

}
