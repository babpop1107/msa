package com.jogym.api.customer.model;

import com.jogym.api.customer.entity.Customer;
import com.jogym.common.interfaces.CommonModelBuilder;
import com.jogym.common.function.CommonFormat;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomerResponse {

    // 등록일, 회원명, 연락처, 주소, 성별, 생년월일, 비고
    private String dateCreate;
    private String name;
    private String phoneNumber;
    private String address;
    private String gender;
    private LocalDate dateBirth;
    private String memo;

    private CustomerResponse(Builder builder){
        this.dateCreate = builder.dateCreate;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.memo = builder.memo;
    }


    public static class Builder implements CommonModelBuilder<CustomerResponse>{
        private final String dateCreate;
        private final String name;
        private final String phoneNumber;
        private final String address;
        private final String gender;
        private final LocalDate dateBirth;
        private final String memo;

        public Builder(Customer customer){
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(customer.getDateCreate());
            this.name = customer.getName();
            this.phoneNumber = customer.getPhoneNumber();
            this.address = customer.getAddress();
            this.gender = customer.getGender().getName();
            this.dateBirth = customer.getDateBirth();
            this.memo = customer.getMemo();
        }

        @Override
        public CustomerResponse build() {
            return new CustomerResponse(this);
        }
    }

}
