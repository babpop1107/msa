package com.jogym.common.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
