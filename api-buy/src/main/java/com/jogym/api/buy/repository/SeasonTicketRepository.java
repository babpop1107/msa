package com.jogym.api.buy.repository;

import com.jogym.api.buy.entity.SeasonTicket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeasonTicketRepository extends JpaRepository<SeasonTicket, Long> {

}
