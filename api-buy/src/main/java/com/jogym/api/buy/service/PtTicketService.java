package com.jogym.api.buy.service;

import com.jogym.api.buy.entity.PtTicket;
import com.jogym.api.buy.repository.PtTicketRepository;
import com.jogym.common.exception.CMissingDataException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PtTicketService {
    private final PtTicketRepository ptTicketRepository;

    public PtTicket getPtTicketData(long ptTicketId){
        return ptTicketRepository.findById(ptTicketId).orElseThrow(CMissingDataException::new);
    }

}
