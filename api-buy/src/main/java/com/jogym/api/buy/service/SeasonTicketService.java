package com.jogym.api.buy.service;

import com.jogym.api.buy.entity.SeasonTicket;
import com.jogym.api.buy.repository.SeasonTicketRepository;
import com.jogym.common.exception.CMissingDataException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SeasonTicketService {
    private final SeasonTicketRepository seasonTicketRepository;

    public SeasonTicket getOriginSeasonTicket(long id) {
        return seasonTicketRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

}
