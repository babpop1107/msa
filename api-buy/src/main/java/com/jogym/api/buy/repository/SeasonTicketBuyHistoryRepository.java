package com.jogym.api.buy.repository;

import com.jogym.api.buy.entity.Customer;
import com.jogym.api.buy.entity.SeasonTicketBuyHistory;
import com.jogym.api.buy.entity.StoreMember;
import com.jogym.common.enums.BuyStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SeasonTicketBuyHistoryRepository extends JpaRepository<SeasonTicketBuyHistory, Long> {

    long countByCustomerAndBuyStatus(Customer customer, BuyStatus buyStatus);

    Optional<SeasonTicketBuyHistory> findByCustomer_StoreMemberAndIdAndBuyStatus(StoreMember storeMember, Long id, BuyStatus buyStatus);

    Optional<SeasonTicketBuyHistory> findByCustomer_StoreMemberAndId(StoreMember storeMember, Long id);

    Page<SeasonTicketBuyHistory> findAllByCustomer_IdAndCustomer_StoreMemberOrderByIdDesc(Long memberId, StoreMember storeMember, Pageable pageable);

}
