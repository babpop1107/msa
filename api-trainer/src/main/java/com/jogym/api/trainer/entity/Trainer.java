package com.jogym.api.trainer.entity;

import com.jogym.common.enums.Gender;
import com.jogym.common.interfaces.CommonModelBuilder;
import com.jogym.api.trainer.model.TrainerRequest;
import com.jogym.api.trainer.model.TrainerUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Trainer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원등록일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 가맹점id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "storeMemberId", nullable = false)
    private StoreMember storeMember;

    // 이름
    @Column(nullable = false, length = 20)
    private String name;

    // 연락처
    @Column(nullable = false, length = 13)
    private String phoneNumber;

    // 주소
    @Column(nullable = false, length = 100)
    private String address;

    // 성별
    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    // 생년월일
    @Column(nullable = false)
    private LocalDate dateBirth;

    // 경력 및 자격사항
    @Column(columnDefinition = "TEXT")
    private String careerContent;

    // 비고
    @Column(columnDefinition = "TEXT")
    private String memo;

    // 사용유무
    @Column(nullable = false)
    private Boolean isEnabled;

    public void putTrainer(TrainerUpdateRequest request) {
        this.name = request.getName();
        this.phoneNumber = request.getPhoneNumber();
        this.address = request.getAddress();
        this.gender = request.getGender();
        this.dateBirth = request.getDateBirth();
        this.careerContent = request.getCareerContent();
        this.memo = request.getMemo();
    }

    // 트레이너 활성화 변경 --> true에서 false로 변경
    // 트레이너 삭제
    public void putTrainerDelete() {
        this.isEnabled = false;
    }


    private Trainer(TrainerBuilder builder) {
        this.dateCreate = builder.dateCreate;
        this.storeMember = builder.storeMember;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.isEnabled = builder.isEnabled;
        this.careerContent = builder.careerContent;
        this.memo = builder.memo;
    }

    public static class TrainerBuilder implements CommonModelBuilder<Trainer> {
        private final LocalDateTime dateCreate;
        private final StoreMember storeMember;
        private final String name;
        private final String phoneNumber;
        private final String address;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final Boolean isEnabled;
        private final String careerContent;
        private final String memo;

        public TrainerBuilder(StoreMember storeMember, TrainerRequest request) {
            this.dateCreate = LocalDateTime.now();
            this.storeMember = storeMember;
            this.name = request.getName();
            this.phoneNumber = request.getPhoneNumber();
            this.address = request.getAddress();
            this.gender = request.getGender();
            this.dateBirth = request.getDateBirth();
            this.isEnabled = true;
            this.careerContent = request.getCareerContent();
            this.memo = request.getMemo();
        }
        @Override
        public Trainer build() {
            return new Trainer(this);
        }
    }
}
