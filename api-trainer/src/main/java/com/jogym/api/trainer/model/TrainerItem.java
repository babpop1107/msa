package com.jogym.api.trainer.model;

import com.jogym.api.trainer.entity.Trainer;
import com.jogym.common.interfaces.CommonModelBuilder;
import com.jogym.common.function.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TrainerItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(notes = "등록일", required = true)
    @NotNull
    private String dateCreate;

    @ApiModelProperty(notes = "이름", required = true)
    @NotNull
    private String name;

    @ApiModelProperty(notes = "연락처", required = true)
    @NotNull
    private String phoneNumber;

    private TrainerItem(Builder builder) {
        this.id = builder.id;
        this.dateCreate = builder.dateCreate;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
    }

    public static class Builder implements CommonModelBuilder<TrainerItem> {
        private final Long id;
        private final String dateCreate;
        private final String name;
        private final String phoneNumber;

        public Builder(Trainer trainer) {
            this.id = trainer.getId();
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(trainer.getDateCreate());
            this.name = trainer.getName();
            this.phoneNumber = trainer.getPhoneNumber();
        }
        @Override
        public TrainerItem build() {
            return new TrainerItem(this);
        }
    }
}
