package com.jogym.common.exception;

public class CNotExistMemberOfStoreMemberException extends RuntimeException {
    public CNotExistMemberOfStoreMemberException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotExistMemberOfStoreMemberException(String msg) {
        super(msg);
    }

    public CNotExistMemberOfStoreMemberException() {
        super();
    }
}
