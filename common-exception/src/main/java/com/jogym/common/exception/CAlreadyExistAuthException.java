package com.jogym.common.exception;

public class CAlreadyExistAuthException extends RuntimeException {
    public CAlreadyExistAuthException(String msg, Throwable t) {
        super(msg, t);
    }

    public CAlreadyExistAuthException(String msg) {
        super(msg);
    }

    public CAlreadyExistAuthException() {
        super();
    }
}