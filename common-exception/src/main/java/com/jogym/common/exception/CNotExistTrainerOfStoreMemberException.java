package com.jogym.common.exception;

public class CNotExistTrainerOfStoreMemberException extends RuntimeException {
    public CNotExistTrainerOfStoreMemberException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotExistTrainerOfStoreMemberException(String msg) {
        super(msg);
    }

    public CNotExistTrainerOfStoreMemberException() {
        super();
    }
}
