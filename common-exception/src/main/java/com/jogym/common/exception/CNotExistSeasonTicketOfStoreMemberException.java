package com.jogym.common.exception;

public class CNotExistSeasonTicketOfStoreMemberException extends RuntimeException {
    public CNotExistSeasonTicketOfStoreMemberException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotExistSeasonTicketOfStoreMemberException(String msg) {
        super(msg);
    }

    public CNotExistSeasonTicketOfStoreMemberException() {
        super();
    }
}
