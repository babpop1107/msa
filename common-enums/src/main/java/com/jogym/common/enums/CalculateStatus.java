package com.jogym.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CalculateStatus {
    ING("정산중"),
    COMPLETE("정산완료");

    private final String name;
    }
